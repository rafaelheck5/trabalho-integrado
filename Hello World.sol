pragma solidity ^0.4.0;

contract HelloWorld{
  string word = "Hello World";
  address issuer;
  
    function HelloWorld(){
      issuer = msg.sender;
  }
    modifier IfIssuer(){
        if (issuer !=msg.sender){
            throw;
            }
        else{
            _;
        }
    }
    function getWord() constant returns(string){
        return word;
    }
    function SetWord(string newWord) IfIssuer returns(string){
        word = newWord;
        return "This is the creator";
    }
    
}
